using CourseProjectApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using Npgsql;
using Microsoft.AspNetCore.Mvc;


namespace CourseProjectApp.Data
{
    public class CourseProjectAppContext : DbContext
    {
        public CourseProjectAppContext (
            DbContextOptions<CourseProjectAppContext> options)
            : base(options)
        {
        }
       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
        }


        public DbSet<Product> Products { get; set; }
        public DbSet<Attribute> Attributes { get; set; }
        public DbSet<AttributeGroup> AttributeGroups { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CategoryName> CategoryNames { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<LocalAttributeGroupeName> LocalAttributeGroupeNames { get; set; }
        public DbSet<LocalAttributeName> LocalAttributeNames { get; set; }
        public DbSet<Locale> Locales { get; set; }


    }
}