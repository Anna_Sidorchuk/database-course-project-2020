﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CourseProjectApp.Migrations
{
    public partial class product_image_brand : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Img",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "BrandId",
                table: "Brands");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Products",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddColumn<string>(
                name: "Img_link",
                table: "Images",
                type: "character varying(256)",
                maxLength: 256,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Order",
                table: "Images",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "BrandName",
                table: "Brands",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "Product_Id_Index",
                table: "Products",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Order_Id_Index",
                table: "Images",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Order_Image_Index",
                table: "Images",
                column: "Order",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Brand_Id_Index",
                table: "Brands",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Brand_Name_Index",
                table: "Brands",
                column: "BrandName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "Product_Id_Index",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "Order_Id_Index",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "Order_Image_Index",
                table: "Images");

            migrationBuilder.DropIndex(
                name: "Brand_Id_Index",
                table: "Brands");

            migrationBuilder.DropIndex(
                name: "Brand_Name_Index",
                table: "Brands");

            migrationBuilder.DropColumn(
                name: "Img_link",
                table: "Images");

            migrationBuilder.DropColumn(
                name: "Order",
                table: "Images");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Products",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(128)",
                oldMaxLength: 128);

            migrationBuilder.AddColumn<string>(
                name: "Img",
                table: "Images",
                type: "text",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BrandName",
                table: "Brands",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "character varying(128)",
                oldMaxLength: 128);

            migrationBuilder.AddColumn<int>(
                name: "BrandId",
                table: "Brands",
                type: "integer",
                nullable: true);
        }
    }
}
