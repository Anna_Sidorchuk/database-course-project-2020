﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProjectApp.Models
{
    public class Category
    {
        // НЕ РЕАЛИЗОВАНО ДЕРЕВО/ ИЛИ ССЫЛКА НА САМОГО СЕБЯ
        public int Id { get; set; }
       
        


        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }


        public virtual ICollection<CategoryName> CategoryNames { get; set; }
        public virtual ICollection<AttributeGroup> AttributeGroups { get; set; }


    }
}
