﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Npgsql;
using Npgsql.EntityFrameworkCore.PostgreSQL.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace CourseProjectApp.Models
{
    [Index("Order", IsUnique = true, Name = "Order_Image_Index")]
    [Index("Id", IsUnique = true, Name = "Order_Id_Index")]
    public class Image 
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(256, ErrorMessage = "Недопустимая длина")]
        public string Img_link { get; set; }

        public int Order { get; set; }

        public int? ProductId { get; set; }
        public virtual Product Product{ get; set; }
    }


}

