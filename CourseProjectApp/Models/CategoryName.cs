﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace CourseProjectApp.Models
{
    public class CategoryName

    {
        [Key]
        public int Id { get; set; }

        public string CategName { get; set; }

        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
