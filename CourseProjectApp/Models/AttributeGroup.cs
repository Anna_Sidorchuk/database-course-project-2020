﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseProjectApp.Models
{
    public class AttributeGroup
    {
        public int Id { get; set; }

        //public int AttributeGroupeNameId { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<LocalAttributeGroupeName> LocalAttributeGroupeNames { get; set; }

      
    }
}
