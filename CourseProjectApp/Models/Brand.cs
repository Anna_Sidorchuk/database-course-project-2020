﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;


namespace CourseProjectApp.Models
{
    [Index("Id", IsUnique = true, Name = "Brand_Id_Index")]
    [Index("BrandName", IsUnique = true, Name = "Brand_Name_Index")]

    public class Brand
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(128, ErrorMessage = "Недопустимая длина")]
        public string BrandName { get; set; }


        public int? ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
