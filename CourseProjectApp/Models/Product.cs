using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CourseProjectApp.Models
{
    [Index("Id", IsUnique = true, Name = "Product_Id_Index")]
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(128, ErrorMessage = "Недопустимая длина")]
        public string Name { get; set; }


        public virtual ICollection<Image> Images { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<Brand> Brands { get; set; }


    }
}
