# Database Course Project 2020
Проект представляет собой монолит на ASP.NET Core с использованием Razor Pages и EntityFramework Core.

## Запуск проекта
#### Пререквизиты
- .NET 5.0 SDK  ([тык](https://dotnet.microsoft.com/download))
- PostgreSQL 13 ([тык](https://www.postgresql.org/download/)).
- Visual Studio Code ([тык](https://code.visualstudio.com/download))

#### NB! Перед первым запуском
- Надо установить доверие к сертификату разработки HTTPS:
```dotnet dev-certs https --trust```

#### Консольные команды для запуска
- ```dotnet run``` - запускает дев версию проекта. Доступно по адресу https://localhost:4000
- ```dotnet watch run``` - запуск проекта с вотчером изменений
- ```dotnet build``` - сборка проекта

## Полезные ссылки
- Документация по ASP.NET ([тык](https://docs.microsoft.com/ru-ru/aspnet/core/?view=aspnetcore-5.0))
- Документация по Razor Pages ([тык](https://docs.microsoft.com/ru-ru/aspnet/core/tutorials/razor-pages/?view=aspnetcore-5.0))
- Документаци по EF Core ([тык](https://docs.microsoft.com/ru-ru/ef/))
